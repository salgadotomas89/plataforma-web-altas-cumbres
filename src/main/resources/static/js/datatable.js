$(document).ready(function(){
    var table = $('#filesTable').DataTable({
        "sAjaxSource": "/board",
        "sAjaxDataProp": "",
        "order": [[ 0, "asc" ]],
        "aoColumns": [
            { "mData": "des"},
            { "mData": "fecha" },
            { "mData": "comentario" },
            { "mData": "tipo" },
            { "mData": "categoria" },
            { "mData": "materia" },
            { "mData": "estado" }
        ]
    })

});

