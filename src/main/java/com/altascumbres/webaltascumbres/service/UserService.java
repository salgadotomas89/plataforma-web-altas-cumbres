package com.altascumbres.webaltascumbres.service;

import com.altascumbres.webaltascumbres.entity.User;
import com.altascumbres.webaltascumbres.repository.AuthorityRepository;
import com.altascumbres.webaltascumbres.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.*;

@Service
public class UserService {

    @Autowired
    public UserRepository userRepository;
    @Autowired
    private AuthorityRepository authorityRepository;

    public Object usuarios = null;
    public boolean usuarioNuevo = false;


    public Object getAll() {
        if (usuarios == null || usuarioNuevo){
            usuarios = userRepository.findAll();
        }
        return usuarios;
    }

    public User get(Long id) {
        return userRepository.getById(id);
    }

    public void save(User user) {
        encriptarPassword(user);
        user.setNotificacion(0);
        user.setEnabled(true);
        switch (user.getArea()){
            case "admin" : user.setAuthority(new HashSet<>(authorityRepository.findAllById(Collections.singleton((long) 1))));
            case "user" : user.setAuthority(new HashSet<>(authorityRepository.findAllById(Collections.singleton((long) 2))));
            case "profesor" : user.setAuthority(new HashSet<>(authorityRepository.findAllById(Collections.singleton((long) 3))));
        }
        userRepository.save(user);
        usuarioNuevo = true;
    }

    public void delete(Long id) {
        userRepository.deleteById(id);
    }

    public User findByUsername(String username) {
        return userRepository.findByUsername(username);
    }

    private void encriptarPassword(User user){
        String pass = user.getPassword();
        BCryptPasswordEncoder bCryptPasswordEncoder = new BCryptPasswordEncoder(4);
        user.setPassword(bCryptPasswordEncoder.encode(pass));
    }

    @Transactional
    public void updateNotification(Long id, int notify ) {
        userRepository.updateNotification(id, notify);
    }

    public User buscaridUsuarioLogeado(){
        Object principal = SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        UserDetails userDetails = null;
        if (principal instanceof UserDetails) {
            userDetails = (UserDetails) principal;
        }
        if(userDetails == null){
            return null;
        }else {
            return findByUsername(userDetails.getUsername());
        }
    }

    public List<User> getAllProfesores() {
        return userRepository.getAllProfesores();
    }

    @Transactional
    public void actualizarUltimaConexion(Long id, Date date){
        userRepository.actualizarUltimaConexion(id, date);
    }
}
