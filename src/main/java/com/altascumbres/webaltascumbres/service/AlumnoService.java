package com.altascumbres.webaltascumbres.service;


import com.altascumbres.webaltascumbres.entity.Alumno;
import com.altascumbres.webaltascumbres.entity.Asignatura;
import com.altascumbres.webaltascumbres.entity.Nota;
import com.altascumbres.webaltascumbres.entity.User;
import com.altascumbres.webaltascumbres.repository.AlumnoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class AlumnoService {

    @Autowired
    private AlumnoRepository alumnoRepository;

    public Object getAll() {
        return alumnoRepository.findAll();
    }

    public Object getByCurso(Long id) {
        return alumnoRepository.getByCurso(id);
    }

    public void save(Alumno alumno){
        alumnoRepository.save(alumno);
    }

    public void deleteAsignatura(Alumno alumno, Asignatura asignatura) {
        Alumno alumno1 = alumnoRepository.getOne(alumno.getId());
        List<Nota> notas = alumno1.getNotas();
        for (Nota nota: notas){
            if(nota.getAsignatura().equals(asignatura)){
                notas.remove(nota);
            }
        }
        alumno1.setNotas(notas);
        alumnoRepository.save(alumno);
    }


}
