package com.altascumbres.webaltascumbres.service;


import com.altascumbres.webaltascumbres.entity.Asignatura;
import com.altascumbres.webaltascumbres.entity.Nota;
import com.altascumbres.webaltascumbres.repository.NotaRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;

@Service
public class NotaService {

    @Autowired
    public NotaRepository notaRepository;

    @Transactional
    public void guardarNota(Long id, Double valor) {
        notaRepository.updateNota(id,valor);
    }

    public void save(Nota nota) {
        notaRepository.save(nota);
    }

    public void delete(Nota nota) {
        notaRepository.deleteById(nota.getId());
    }
}
