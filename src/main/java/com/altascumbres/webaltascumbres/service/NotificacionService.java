package com.altascumbres.webaltascumbres.service;

import com.altascumbres.webaltascumbres.entity.Notificacion;
import com.altascumbres.webaltascumbres.repository.NotificacionRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class NotificacionService {

    @Autowired
    public NotificacionRepository notificacionRepository;

    public void guardar(Notificacion notificacion){
        notificacionRepository.save(notificacion);
    }

    public List<Notificacion> getNotificacionByIdUser(Long id) {
        return notificacionRepository.findAllByIdUsuario(id);
    }

    public void deleteAll() {
        notificacionRepository.deleteAll();
    }
}
