package com.altascumbres.webaltascumbres.service;

import com.altascumbres.webaltascumbres.entity.Archivo;
import com.altascumbres.webaltascumbres.entity.User;
import com.altascumbres.webaltascumbres.repository.ArchivoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.JavaMailSenderImpl;
import org.springframework.stereotype.Service;
import javax.transaction.Transactional;
import java.util.Collections;
import java.util.List;
import java.util.Properties;


@Service
public class ArchivoService {
    @Autowired
    public ArchivoRepository archivoRepository;
    @Autowired
    private JavaMailSender javaMailSender;

    public Object getAll() {
        return archivoRepository.findAll();
    }

    public Object getAllByNivel(Long nivel){
        return  archivoRepository.findAllByNivel(nivel);
    }

    public Object getAllByNivelAndTipo(Long nivel, String tipo){
        return archivoRepository.findAllByNivelAndTipo(nivel, tipo);
    }

    public Object getAllByNivelTipoId(Long nivel, String tipo, Long id){
        return archivoRepository.findAllByNivelTipoId(nivel, tipo, id);
    }

    public Archivo get(Long id) {
        return archivoRepository.findById(id).get();
    }

    public void save(Archivo archivo, String nombre) {
        archivo.setEstado("sin revisar");
        archivoRepository.save(archivo);
        String mensaje = nombre + " ha subido un archivo en planificaciones";
        sendEmail(mensaje, "mane-1981@hotmail.com");
    }

    void sendEmail(String message, String receptor) {
        SimpleMailMessage msg = new SimpleMailMessage();
        msg.setTo(receptor);
        msg.setSubject("Mensaje de Plataforma");
        msg.setText(message);
        javaMailSender.send(msg);
    }

    public void delete(Long id) {
        archivoRepository.deleteById(id);
    }

    @Transactional
    public void updateComentario(String comentario, Long id){
        archivoRepository.updateComentario(id, comentario);
    }

    public Object getAllByNivelId(Long nivelCurso, Long idUsuario) {
        return archivoRepository.findAllByNivelId(nivelCurso, idUsuario);
    }

    @Transactional
    public void updateEstado(Long id, String estado, User user) {
        if(estado.equals("aprobado")){
            sendEmail("Uno de sus archivos ha sido aprobado", user.getEmail());
        }else if(estado.equals("rechazado")){
            sendEmail("Uno de sus archivos ha sido rechazado", user.getEmail());
        }
        archivoRepository.updateEstado(id, estado);
    }

    @Transactional
    public void updateNombreArchivo(Archivo archivo) {
        archivoRepository.updateNombreArchivo(archivo.getId(), archivo.getNombreArchivo(), "actualizado");
    }

    /*
    @Transactional
    public void updateArchivo(Long id, Archivo archivo) {
        archivoRepository.updateArchivoBytes(id, archivo.getArchivo(), archivo.getContentType(), archivo.getNombreArchivo());
    }*/


}
