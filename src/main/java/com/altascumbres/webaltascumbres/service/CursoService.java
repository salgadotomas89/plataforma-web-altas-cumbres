package com.altascumbres.webaltascumbres.service;

import com.altascumbres.webaltascumbres.entity.Asignatura;
import com.altascumbres.webaltascumbres.entity.Curso;
import com.altascumbres.webaltascumbres.entity.User;
import com.altascumbres.webaltascumbres.repository.CursoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class CursoService {
    @Autowired
    public CursoRepository cursoRepository;

    public Object cursos = null;
    public boolean cursoNuevo = false;

    public Object getAll() {
        if(cursos == null || cursoNuevo) {
            cursos = cursoRepository.findAll();
        }
        return cursos;
    }

    public Curso get(Long id) {
        return cursoRepository.getByID(id);
    }

    public void save(Curso curso) {
        cursoRepository.save(curso);
        cursoNuevo = true;
    }

    public void delete(Long id) {
        cursoRepository.deleteById(id);
    }

    public Curso getById(Long id){
        return cursoRepository.getByID(id);
    }


    public Object getCursos(User usuarioElegido) {
        switch (usuarioElegido.getCiclo()){
            //si es 1, es solo de primer ciclo
            case 1 : return cursoRepository.getByIdProf(usuarioElegido.getId());
            //si es 2 es solo de segundo ciclo
            case 2: return cursoRepository.getSecond();
            //si es 3 es de ambos
            case 3: return cursoRepository.findAll();
            default:
                throw new IllegalStateException("Unexpected value: " + usuarioElegido.getCiclo());
        }
    }


    public Curso getByIdProf(Long id) {
        return cursoRepository.getByIdProf(id);
    }

}
