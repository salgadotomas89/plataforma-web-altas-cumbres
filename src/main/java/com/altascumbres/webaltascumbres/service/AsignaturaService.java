package com.altascumbres.webaltascumbres.service;

import com.altascumbres.webaltascumbres.repository.AsignaturaRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class AsignaturaService {

    @Autowired
    private AsignaturaRepository asignaturaRepository;

    public Object getAll() {
        return asignaturaRepository.findAll();
    }

    //devuelve una asignatura por el id entregado como parametro
    public Object get(Long asigId) {

        return asignaturaRepository.getOne(asigId);
    }
}
