package com.altascumbres.webaltascumbres.service;

import com.altascumbres.webaltascumbres.entity.FileArc;
import com.altascumbres.webaltascumbres.repository.DatoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;

@Service
public class DatoService {
    @Autowired
    DatoRepository datoRepository;

    public void save(FileArc fileArc){
        datoRepository.save(fileArc);
    }

    public FileArc get(Long id) {
        return datoRepository.findById(id).get();
    }

    @Transactional
    public void udpdatefileArc(Long idArchivoActualizar, FileArc fileArc) {
        datoRepository.updateFileArc(idArchivoActualizar, fileArc.getContentType(), fileArc.getDatosDelArchivo(), fileArc.getNombreArchivo());
    }

    public void delete(Long id) {
        datoRepository.deleteById(id);
    }
}
