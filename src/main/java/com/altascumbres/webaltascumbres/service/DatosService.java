package com.altascumbres.webaltascumbres.service;


import com.altascumbres.webaltascumbres.repository.DatosRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class DatosService {

    @Autowired
    private DatosRepository datosRepository;


    public Object getAll(){
        return datosRepository.findAll();
    }

}
