package com.altascumbres.webaltascumbres.service;

import com.altascumbres.webaltascumbres.WebaltascumbresApplication;
import com.altascumbres.webaltascumbres.entity.Fecha;
import com.altascumbres.webaltascumbres.entity.Recurso;
import com.altascumbres.webaltascumbres.entity.User;
import com.altascumbres.webaltascumbres.repository.RecursoRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.FileSystemResource;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Service;

import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;
import javax.transaction.Transactional;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.stream.Collectors;

@Service
public class RecursoService {
    @Autowired
    RecursoRepository recursoRepository;
    @Autowired
    private JavaMailSender javaMailSender;
    private List<Fecha> fechaList;
    private Logger logger = LoggerFactory.getLogger(WebaltascumbresApplication.class);


    public void save(Recurso recurso, String nombre){
        recurso.setEstado("sin revisar");
        recursoRepository.save(recurso);
        String mensaje = nombre + " ha subido un recurso nuevo ";
        sendEmail(mensaje, "mane-1981@hotmail.com");
    }

    void sendEmail(String message, String receptor) {
        SimpleMailMessage msg = new SimpleMailMessage();
        msg.setTo(receptor);
        msg.setSubject("Mensaje de Plataforma");
        msg.setText(message);
        javaMailSender.send(msg);
    }

    public List<Recurso> getAllByNivelId(Long idCursoElegido, Long idUsuarioElegido) {
        return recursoRepository.getAllByNivelId(idCursoElegido, idUsuarioElegido);
    }


    @Transactional
    public void updateNombreArchivo(Recurso archivo) {
        recursoRepository.updateNombreArchivo(archivo.getId(), archivo.getNombreArchivo(), "actualizado");
    }

    @Transactional
    public void updateEstado(Long id, String estado, User user) {
        if(estado.equals("aprobado")){
            sendEmail("Uno de sus archivos ha sido aprobado", user.getEmail());
        }else if(estado.equals("rechazado")){
            sendEmail("Uno de sus archivos ha sido rechazado", user.getEmail());
        }
        recursoRepository.updateEstado(id, estado);
    }

    public void delete(Long id) {
        recursoRepository.deleteById(id);
    }

    public Recurso get(Long idArchivoActualizar) {
        return recursoRepository.findById(idArchivoActualizar).get();
    }

    @Transactional
    public void updateComentario(String texto, Long idArchivoModificado) {
        recursoRepository.updateComentario(idArchivoModificado, texto);
    }

    public List<Fecha> getFechasRecursos() {
        fechaList = new ArrayList<>();

        //Traigo todas las  fechas de los lunes
        List<Date> listaFechas = recursoRepository.getFechas();

        //creo una nueva lista con los elementos sin repetir
        List<Date> newList = listaFechas.stream()
                .distinct()
                .collect(Collectors.toList());

        //Recorro
        for (Date fecha : newList){
            logger.info("FECHA "+fecha);

            if(fecha != null) {
                Fecha f = new Fecha();
                f.setFechaDate(fecha);
                DateFormat dateFormat = new SimpleDateFormat("d MMM, yyyy");
                f.setFecha(dateFormat.format(fecha));
                fechaList.add(f);
            }
        }

        return fechaList;
    }

    public Object getAllByNivelFecha(Long idCursoElegido, Date fechaElegida) {
        List<Recurso> listaRecursos = recursoRepository.getRecurosNivelFecha(idCursoElegido, fechaElegida);

        return listaRecursos;
    }

    public void sendFile(Long id) throws MessagingException {

        MimeMessage message = javaMailSender.createMimeMessage();

// use the true flag to indicate you need a multipart message
        MimeMessageHelper helper = new MimeMessageHelper(message, true);
        helper.setTo("salgadotomas@outlook.com");

        helper.setText("Check out this image!");
        Recurso recurso = recursoRepository.getOne(id);


// let's attach the infamous windows Sample file (this time copied to c:/)
        FileSystemResource file = new FileSystemResource(new File("c:/Sample.jpg"));
        helper.addAttachment("CoolImage.jpg", file);

        javaMailSender.send(message);

    }
}
