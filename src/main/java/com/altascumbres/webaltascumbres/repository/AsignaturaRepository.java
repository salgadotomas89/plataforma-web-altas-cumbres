package com.altascumbres.webaltascumbres.repository;

import com.altascumbres.webaltascumbres.entity.Asignatura;
import org.springframework.data.jpa.repository.JpaRepository;

public interface AsignaturaRepository extends JpaRepository<Asignatura, Long> {
}
