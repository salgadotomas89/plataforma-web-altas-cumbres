package com.altascumbres.webaltascumbres.repository;

import com.altascumbres.webaltascumbres.entity.Datos;
import org.springframework.data.jpa.repository.JpaRepository;

public interface DatosRepository extends JpaRepository<Datos, Long> {
}
