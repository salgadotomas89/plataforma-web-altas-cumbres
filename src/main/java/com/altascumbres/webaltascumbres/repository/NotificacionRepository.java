package com.altascumbres.webaltascumbres.repository;

import com.altascumbres.webaltascumbres.entity.Notificacion;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface NotificacionRepository extends JpaRepository<Notificacion, Long> {

    @Query("select c from Notificacion c where c.idUsuario = :id")
    List<Notificacion> findAllByIdUsuario(Long id);
}
