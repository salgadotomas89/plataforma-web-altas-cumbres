package com.altascumbres.webaltascumbres.repository;

import com.altascumbres.webaltascumbres.entity.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;

import java.util.Date;
import java.util.List;


public interface UserRepository extends JpaRepository<User,Long> {
    @Query("select c from User c where c.username = ?1")
    User findByUsername( String username);

    @Query("select c from User c where c.id = ?1")
    User getById(Long id);

    @Modifying
    @Query("update User v set v.notificacion = ?2 where v.id = ?1 ")
    void updateNotification(Long id, int notificacion);

    @Query("select c from User c where c.area = 'profesor' and c.estado = true ")
    List<User> getAllProfesores();

    @Modifying
    @Query("update User v set v.ultimaConexion = ?2 where v.id = ?1 ")
    void actualizarUltimaConexion(Long appUser, Date date);
}
