package com.altascumbres.webaltascumbres.repository;

import com.altascumbres.webaltascumbres.entity.Archivo;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface ArchivoRepository extends JpaRepository<Archivo, Long> {

    @Query("select c from Archivo c where c.nivel = :nivel")
    List<Archivo> findAllByNivel(@Param("nivel") Long nivel);

    @Query("select c from Archivo c where c.nivel = :nivel and c.tipo = :tipo")
    List<Archivo> findAllByNivelAndTipo(@Param("nivel") Long nivel, String tipo);

    @Query("select c from Archivo c where c.nivel = :nivel and c.tipo = :tipo and c.idUsuario = :id")
    List<Archivo> findAllByNivelTipoId(@Param("nivel") Long nivel, String tipo,@Param("id") Long id);

    @Query("select c from Archivo c where c.nivel = ?1 and c.idUsuario = ?2")
    List<Archivo> findAllByNivelId(Long nivel, Long id);

    @Modifying
    @Query("update Archivo v set v.estado = ?2 where v.id = ?1 ")
    void updateEstado(Long id, String estado);

    @Modifying
    @Query("update Archivo u set u.nombreArchivo = ?2, u.estado = ?3 where u.id = ?1")
    void updateNombreArchivo(Long id, String nombreArchivo, String actualizado);

    @Modifying
    @Query("update Archivo u set u.comentario = ?2 where u.id = ?1")
    void updateComentario(Long id, String comentario);
}
