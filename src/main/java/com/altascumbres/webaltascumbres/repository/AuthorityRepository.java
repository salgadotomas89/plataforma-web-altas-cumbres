package com.altascumbres.webaltascumbres.repository;

import com.altascumbres.webaltascumbres.entity.Authority;
import org.springframework.data.jpa.repository.JpaRepository;

public interface AuthorityRepository extends JpaRepository<Authority, Long> {

}
