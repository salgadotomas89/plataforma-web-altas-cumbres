package com.altascumbres.webaltascumbres.repository;

import com.altascumbres.webaltascumbres.entity.Recurso;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;

import java.util.Date;
import java.util.List;

public interface RecursoRepository extends JpaRepository<Recurso, Long> {

    @Query("select c from Recurso c where c.nivel = ?1 and c.idUsuario = ?2 ")
    List<Recurso> getAllByNivelId(Long idCursoElegido, Long idUsuarioElegido);

    @Modifying
    @Query("update Recurso v set v.estado = ?2 where v.id = ?1 ")
    void updateEstado(Long id, String aprobado);

    @Modifying
    @Query("update Recurso u set u.nombreArchivo = ?2, u.estado = ?3 where u.id = ?1")
    void updateNombreArchivo(Long id, String nombreArchivo, String actualizado);

    @Modifying
    @Query("update Recurso u set u.comentario = ?2 where u.id = ?1")
    void updateComentario(Long idArchivoModificado, String texto);

    @Query("select fecha from Recurso where estado = 'aprobado' and fecha >= '2021-01-01 00:00:00' ")
    List<Date> getFechas();

    @Query("select c from Recurso c where c.nivel = ?1 and c.fecha = ?2")
    List<Recurso> getRecurosNivelFecha(Long idCursoElegido, Date date);
}
