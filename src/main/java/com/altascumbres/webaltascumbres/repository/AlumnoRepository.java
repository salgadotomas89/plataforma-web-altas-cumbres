package com.altascumbres.webaltascumbres.repository;

import com.altascumbres.webaltascumbres.entity.Alumno;
import com.altascumbres.webaltascumbres.entity.Archivo;
import com.altascumbres.webaltascumbres.entity.Asignatura;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface AlumnoRepository extends JpaRepository<Alumno, Long> {

    @Query("select c from Alumno c where c.curso.id = ?1")
    List<Alumno> getByCurso(Long nivel);

}
