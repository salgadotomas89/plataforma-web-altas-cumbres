package com.altascumbres.webaltascumbres.repository;

import com.altascumbres.webaltascumbres.entity.Asignatura;
import com.altascumbres.webaltascumbres.entity.Curso;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface CursoRepository extends JpaRepository<Curso, Long> {
    @Query("select c from Curso c where c.id = :id")
    Curso getByID(@Param("id") Long id);

    @Query("select c from Curso c where c.idProfesorJefe = ?1 ")
    Curso getByIdProf(Long id);

    @Query("select c from Curso c where c.ciclo = 2 ")
    List<Object> getSecond();


}
