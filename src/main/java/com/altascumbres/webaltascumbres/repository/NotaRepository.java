package com.altascumbres.webaltascumbres.repository;

import com.altascumbres.webaltascumbres.entity.Nota;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;

public interface NotaRepository extends JpaRepository<Nota, Long> {

    @Modifying
    @Query("update Nota v set v.valor = ?2 where v.id = ?1 ")
    void updateNota(Long id, Double valor);



}
