package com.altascumbres.webaltascumbres;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.session.web.context.AbstractHttpSessionApplicationInitializer;

@SpringBootApplication
public class WebaltascumbresApplication extends AbstractHttpSessionApplicationInitializer {


	public static void main(String[] args) {
		BCryptPasswordEncoder bCryptPasswordEncoder = new BCryptPasswordEncoder(4);
		Logger logger = LoggerFactory.getLogger(WebaltascumbresApplication.class);
		logger.info(		bCryptPasswordEncoder.encode("carla22"));
		SpringApplication.run(WebaltascumbresApplication.class, args);

	}


}
