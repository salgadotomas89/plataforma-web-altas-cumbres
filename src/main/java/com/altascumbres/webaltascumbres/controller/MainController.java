package com.altascumbres.webaltascumbres.controller;

import com.altascumbres.webaltascumbres.WebaltascumbresApplication;
import com.altascumbres.webaltascumbres.entity.Alumno;
import com.altascumbres.webaltascumbres.entity.Asignatura;
import com.altascumbres.webaltascumbres.entity.Curso;
import com.altascumbres.webaltascumbres.entity.User;
import com.altascumbres.webaltascumbres.service.AlumnoService;
import com.altascumbres.webaltascumbres.service.AsignaturaService;
import com.altascumbres.webaltascumbres.service.CursoService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.HttpRequestHandler;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import javax.validation.Valid;
import java.util.ArrayList;
import java.util.List;

@Controller
public class MainController {

    @Autowired
    private CursoService cursoService;

    @Autowired
    private AlumnoService alumnoService;

    private Logger logger = LoggerFactory.getLogger(WebaltascumbresApplication.class);





    @GetMapping("/login")
    public String login(){
        return "login";
    }


    @GetMapping("/intranet")
    public String intranet(HttpServletRequest request,HttpSession session) {
        if (request.isUserInRole("ROLE_PROF")) {
            return "redirect:/cursos";
        }else if(request.isUserInRole("ROLE_ADMIN")){
            return "redirect:/usuarios";
        }else {
            return "redirect:/login";
        }
    }



    @RequestMapping("/")
    public String index() {
        return "index";
    }

    @GetMapping("/recuperar")
    public String recuperarPass(Model model) {
        return "recuperar";
    }

    @GetMapping("/send/email")
    public void sendEmail(){

    }




}
