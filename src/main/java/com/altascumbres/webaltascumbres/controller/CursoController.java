package com.altascumbres.webaltascumbres.controller;

import com.altascumbres.webaltascumbres.entity.Curso;
import com.altascumbres.webaltascumbres.service.ArchivoService;
import com.altascumbres.webaltascumbres.service.CursoService;
import com.altascumbres.webaltascumbres.service.NotificacionService;
import com.altascumbres.webaltascumbres.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;

@Controller
public class CursoController {
    @Autowired
    UserService userService;
    @Autowired
    CursoService cursoService;
    @Autowired
    ArchivoService archivoService;
    @Autowired
    NotificacionService notificacionService;

    Long idUsuario;

    @GetMapping("/save/curso/{id}")
    public String guardarCurso(@PathVariable("id") Long id, Model model) {
        if(id != null && id != 0){
            model.addAttribute("curso", cursoService.get(id));
        }else{
            model.addAttribute("curso", new Curso());
            model.addAttribute("listProfesores", userService.getAll());
        }
        return "add-curso";
    }

    @GetMapping("/deletecourse/{id}")
    public String borrarCurso(@PathVariable("id") Long id, Model model) {
        cursoService.delete(id);
        return "redirect:/cursos";
    }

    @PostMapping("/save/curso")
    public String saveCurso(Curso curso, Model model) {
        this.cursoService.save(curso);
        return "redirect:/cursos";
    }


}
