package com.altascumbres.webaltascumbres.controller;

import com.altascumbres.webaltascumbres.WebaltascumbresApplication;
import com.altascumbres.webaltascumbres.entity.*;
import com.altascumbres.webaltascumbres.service.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ByteArrayResource;
import org.springframework.core.io.Resource;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.util.StringUtils;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import javax.validation.Valid;
import java.io.*;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;


@Controller
public class ArchivoController {
    @Autowired
    ArchivoService archivoService;

    @Autowired
    DatoService datoService;

    @Autowired
    DatosService datosService;

    @Autowired
    NotaService notaService;

    @Autowired
    NotificacionService notificacionService;

    @Autowired
    CursoService cursoService;

    @Autowired
    AsignaturaService asignaturaService;

    @Autowired
    RecursoService recursoService;

    @Autowired
    AlumnoService alumnoService;

    @Autowired
    UserService userService;

    private Long idArchivoModificado = null;
    private Long idArchivoActualizar = null;
    //campo Long que contiene el id del usuario elegido por el admin
    private Long idUsuarioElegido = null;
    //campo Long que contiene el id del usuario logeado
    //private User usuarioLogeado = null ;
    private Date fechaElegida = null;
    private  User usuarioElegido = null;
    //campo que contiene el id del curso elegido
    private String tipoArchivo = "todo";
    private Logger logger = LoggerFactory.getLogger(WebaltascumbresApplication.class);



    @GetMapping("/delete/file/{id}")
    public String deleteUser (@PathVariable Long id, Model model, HttpSession session) {
        String tablaVersion = getVerionTabla(session);
        if(tablaVersion.equals("planificaciones")) {
            archivoService.delete(id);
            return "redirect:/board";
        }else{
            recursoService.delete(id);
            return "redirect:/recursos";
        }
    }

    @GetMapping("/send/file")
    public void sendFile(@PathVariable Long id){
        //recursoService.sendFile(id);
    }

    @GetMapping("/approve/state/file/{id}")
    public String apprveFile (@PathVariable Long id, Model model, HttpSession session) {
        String tablaVersion = getVerionTabla(session);

        if(tablaVersion.equals("planificaciones")) {
            archivoService.updateEstado(id, "aprobado",usuarioElegido);
            return "redirect:/board";
        }else {
            recursoService.updateEstado(id, "aprobado", usuarioElegido);
            return "redirect:/recursos";
        }
    }

    @GetMapping("/semanas")
    public String semanas(Model model){
        model.addAttribute("lista",recursoService.getFechasRecursos());
        return "semanas";
    }

    @GetMapping("/refuse/state/file/{id}")
    public String refusefile (@PathVariable Long id, Model model, HttpSession session) {
        String tablaVersion = getVerionTabla(session);

        if(tablaVersion.equals("planificaciones")) {
            archivoService.updateEstado(id, "rechazado", usuarioElegido);
            return "redirect:/board";
        }else {
            recursoService.updateEstado(id, "rechazado", usuarioElegido);
            return "redirect:/recursos";
        }
    }

    @GetMapping("/download/file/{id}")
    public ResponseEntity<Resource> downloadFile(@PathVariable Long id, HttpSession session) throws IOException{
        /*
        Archivo dbFile = archivoService.get(id);
        InputStream targetStream = new FileInputStream(String.valueOf(dbFile.getArchivo()));
        InputStreamResource file = new InputStreamResource(targetStream);

        return ResponseEntity.ok()
                .header(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=\"" + dbFile.getNombreArchivo() + "\"")
                .contentType(MediaType.parseMediaType(dbFile.getContentType()))
                .body(file);*/
        // Load file from database

        String tablaVersion = getVerionTabla(session);


        Archivo dbFile;
        Recurso dbRecurso;
        FileArc fileArc;
        String nombreArchivo;
        if(tablaVersion.equals("planificaciones")){
            dbFile = archivoService.get(id);
            fileArc =  datoService.get(dbFile.getFileArc().getId());
            nombreArchivo = dbFile.getNombreArchivo();
        }else{
            dbRecurso = recursoService.get(id);
            fileArc =  datoService.get(dbRecurso.getFilearc().getId());
            nombreArchivo = dbRecurso.getNombreArchivo();
        }

        return ResponseEntity.ok()
                .contentType(MediaType.parseMediaType(fileArc.getContentType()))
                .header(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=\"" + nombreArchivo + "\"")
                .body(new ByteArrayResource(fileArc.getDatosDelArchivo()));
    }

    @GetMapping("/set/nivel/{nivel}")
    public String cargarNivel(HttpServletRequest request , @PathVariable Long nivel){

        saveNivl(request, nivel);
        return "redirect:/board";
    }



    @GetMapping("/set/nivel/recursos/{nivel}")
    public String cargarNivelRecurso(@PathVariable Long nivel,HttpServletRequest request){
        saveNivl(request, nivel);
        User usuarioLogeado = userService.buscaridUsuarioLogeado();
        if(usuarioLogeado != null){
            return "redirect:/recursos";
        }else{
            return "redirect:/semanas";
        }
    }

    private void saveNivl(HttpServletRequest request, Long nivel){
        Long nvl = getNivel(request.getSession());

        if (!StringUtils.isEmpty(nivel)) {
            nvl = nivel;
            request.getSession().setAttribute("nivel", nvl);
        }
    }

    private Long getNivel(HttpSession session){
        Long nvl = (Long) session.getAttribute("nivel");
        return nvl;
    }


    private void saveVersionTable(HttpServletRequest request, String nivel){
        String nvl = getVerionTabla(request.getSession());

        if (!StringUtils.isEmpty(nivel)) {
            nvl = nivel;
            request.getSession().setAttribute("tabla", nvl);
        }
    }

    private String getVerionTabla(HttpSession session){
        String nvl = (String) session.getAttribute("tabla");
        return nvl;
    }





    @GetMapping("/set/fecha/recursos/{fecha}")
    public String setFechaRecursos(@PathVariable @DateTimeFormat(pattern = "yyyy-MM-dd") Date fecha) {
        //@RequestParam("date") @DateTimeFormat(iso = DateTimeFormat.ISO.DATE)
        //pasamos la fecha elegida en la vista de semanas
        fechaElegida = fecha;
        //vamos a recursos
        return "redirect:/recursos";
    }

    @GetMapping("/set/user/{iduser}")
    public String cargarIdUsuario(@PathVariable Long iduser){
        idUsuarioElegido = iduser;
        usuarioElegido = userService.get(idUsuarioElegido);
        return "redirect:/cursos";
    }

    @GetMapping("/cursos")
    public String cursos(Model model, HttpSession session) {
        logger.info("ID DE SESSION : "+session.getId());
        User usuarioLogeado = userService.buscaridUsuarioLogeado();
        if(usuarioLogeado == null){
            model.addAttribute("lista", cursoService.getAll());//lista con todos los cursos
            model.addAttribute("version", "drgdg");
        }else {
            if (usuarioLogeado.getNotificacion() > 0) {
                model.addAttribute("notificacion", usuarioLogeado.getNotificacion());
                model.addAttribute("comentarios", notificacionService.getNotificacionByIdUser(usuarioLogeado.getId()));
            } else {
                model.addAttribute("notificacion", 0);
            }
            if (usuarioLogeado.getArea().equals("profesor")) {
                Asignatura asignatura = usuarioLogeado.getAsignaturas().get(0);
                model.addAttribute("asignatura", asignatura);
                model.addAttribute("lista", cursoService.getCursos(usuarioLogeado));//lista con todos los cursos
            } else {
                model.addAttribute("lista", cursoService.getCursos(usuarioElegido));//lista con todos los cursos
            }
            model.addAttribute("version", "free");
        }
        model.addAttribute("listaProfesores", userService.getAll());
        model.addAttribute("sessionId", session.getId());
        return "cursos";
    }

    @GetMapping("/settipo/{tipo}")
    public String cargarTipo(@PathVariable String tipo){
        tipoArchivo = tipo;
        return "redirect:/board";
    }

    @GetMapping("/notifications")
    public String notificaciones(Model model) {
        return "notificaciones";
    }






    @GetMapping("/notas/get/asignatura/{id}")
    public String cargarAsignatura(HttpServletRequest request , @PathVariable Long id){
        //busco al usuario
        User usuarioLogeado = userService.buscaridUsuarioLogeado();

        //
        Long cursoId;

        //guardo la asignatura elegida
        saveIdAsig(request, id);

        if(usuarioLogeado.getArea().equals("admin")) {
            cursoId = getNivel(request.getSession());

            Asignatura asignatura = (Asignatura) asignaturaService.get(id);

            List<Alumno> alumnos = (List<Alumno>) alumnoService.getByCurso(cursoId);

        }else {
            //curso elegido
            cursoId = getNivel(request.getSession());


            //Asignatura asignatura = (Asignatura) asignaturaService.get(getIdAsig(request.getSession()));

            Asignatura asignatura = (Asignatura) asignaturaService.get(id);

            List<Alumno> alumnos = (List<Alumno>) alumnoService.getByCurso(cursoId);

            verificarNotas(alumnos, asignatura);
        }

        return "redirect:/notas/"+cursoId;
    }


    @GetMapping("/cargarCurso/{id}")
    public String cargarNivelNotas(HttpServletRequest request,  @PathVariable Long id){
        User usuarioLogeado = userService.buscaridUsuarioLogeado();

        if(usuarioLogeado.getArea().equals("admin")){
            List<Asignatura> asignaturas = userService.get(idUsuarioElegido).getAsignaturas();

            saveIdAsig(request, asignaturas.get(0).getId());

            saveNivl(request,id);

        }else {
            //traigo todas las asignaturas que imparte el profesor
            List<Asignatura> asignaturas = usuarioLogeado.getAsignaturas();

            saveIdAsig(request, asignaturas.get(0).getId());

            saveNivl(request, id);
        }

        return "redirect:/notas/"+id;
    }


    @RequestMapping(value  = "/notas/{cursoId}")
    public String notas( Model model,HttpServletRequest request, @PathVariable("cursoId") Long cursoId){
        saveNivl(request,cursoId);


        //busco al usuario logeado
        User usuarioLogeado = userService.buscaridUsuarioLogeado();

        //veo su identidad
        if (usuarioLogeado.getArea().equals("admin")) {

            Long cursoID = getNivel(request.getSession());

            Asignatura asignatura = (Asignatura) asignaturaService.get(getIdAsig(request.getSession()));

            List<Alumno> alumnos = (List<Alumno>) alumnoService.getByCurso(cursoID);


            //busco los alumnos pertenecientes a ese curso por el id del curso asociado a los alumnos
            model.addAttribute("alumnos", alumnos);

            //cargo las asignaturas que posee
            model.addAttribute("listaAsig", userService.get(idUsuarioElegido).getAsignaturas());


            model.addAttribute("aElegida", asignatura.getNombre());


            model.addAttribute("cantidadNotas", cantidadDeNotas());



        } else if (usuarioLogeado.getArea().equals("profesor")) {
            //si es de primer ciclo
            if (usuarioLogeado.getCiclo() == 1) {

                //buscamos su curso
                Curso curso = cursoService.getByIdProf(usuarioLogeado.getId());

                //buscamos la primera asignatura
                Asignatura asignatura = (Asignatura) asignaturaService.get(getIdAsig(request.getSession()));

                //cargamos a los alumnos de su curso
                List<Alumno> alumnos = (List<Alumno>) alumnoService.getByCurso(curso.getId());

                verificarNotas(alumnos,asignatura);

                model.addAttribute("alumnos", alumnoService.getByCurso(curso.getId()));

                //cargo las asignaturas que posee
                model.addAttribute("listaAsig", usuarioLogeado.getAsignaturas());


                model.addAttribute("aElegida", asignatura.getNombre());

                saveIdAsig(request,asignatura.getId());


                model.addAttribute("cantidadNotas", cantidadDeNotas());

            } else if(usuarioLogeado.getCiclo() == 2){

                //cargo la asignatura o asignaturas dependiendo el profesor
                List<Asignatura> asignaturas = usuarioLogeado.getAsignaturas();

                //cargo los alumnos del curso
                List<Alumno> alumnos = (List<Alumno>) alumnoService.getByCurso(cursoId);


                model.addAttribute("alumnos", alumnos);

                model.addAttribute("aElegida", asignaturas.get(0).getNombre());

                saveIdAsig(request,asignaturas.get(0).getId());

                model.addAttribute("cantidadNotas", cantidadDeNotas());

                //cargo las asiganturas de acuerdo al ciclo que pertenece el profesor
                model.addAttribute("listaAsig", asignaturas);
            }else {
                Asignatura asignatura = (Asignatura) asignaturaService.get(getIdAsig(request.getSession()));

                List<Alumno> alumnos = (List<Alumno>) alumnoService.getByCurso(cursoId);


                model.addAttribute("alumnos", (List<Alumno>) alumnoService.getByCurso(cursoId));

                //cargo las asignaturas que posee
                model.addAttribute("listaAsig", usuarioLogeado.getAsignaturas());


                model.addAttribute("aElegida", asignatura.getNombre());

                saveIdAsig(request,asignatura.getId());


                model.addAttribute("cantidadNotas", cantidadDeNotas());
            }
        }
        return "notas";
    }


    private void calcularPromedio(List<Alumno> alumnos, Asignatura asignatura){
        List<Double> promedios = new ArrayList<>();
        for(Alumno alumno: alumnos){
            Double contador = 0.0;
            Double suma = 0.0;
            List<Nota> notasTotales = alumno.getNotas();
            for (Nota nota : notasTotales){
                if (nota.getAsignatura().equals(asignatura)){
                    if(nota.getValor()!=0.0){
                        suma = suma + nota.getValor();
                        contador++ ;
                    }
                }
            }
        }
    }


    public void verificarNotas(List<Alumno> alumnos, Asignatura asignatura){

        for(Alumno alumno : alumnos){
            List<Nota> notasTotales = alumno.getNotas();

            if(notasTotales.size() == 0){
                logger.info("Notas totales son 0");

                for (int i = 0; i<=3; i++){
                    Nota nota = new Nota();
                    nota.setAsignatura(asignatura);
                    nota.setPeriodo("Primer periodo");
                    nota.setValor(0.0);
                    notaService.save(nota);

                    alumno.getNotas().add(nota);
                    alumnoService.save(alumno);
                }

            }else{
                //hay notas
                int contador = 0;
                //si hay notas recorro cada una para verificar si hay de la asignatura
                for (Nota nota : notasTotales){

                    if (nota.getAsignatura().equals(asignatura)){
                        contador++ ;
                    }
                }


                //si no hay notas de esa asignatura
                if(contador == 0){
                    for (int y = 0; y<=3; y++){
                        Nota nota1 = new Nota();
                        nota1.setAsignatura(asignatura);
                        nota1.setPeriodo("Primer periodo");
                        nota1.setValor(0.0);
                        notaService.save(nota1);

                        alumno.getNotas().add(nota1);
                        alumnoService.save(alumno);
                    }
                }else if(contador < 4){
                    //contador 3
                    int res = 4 - contador;
                    //res 1
                    for (int y = 0; y<res; y++){
                        Nota nota1 = new Nota();
                        nota1.setAsignatura(asignatura);
                        nota1.setPeriodo("Primer periodo");
                        nota1.setValor(0.0);
                        notaService.save(nota1);

                        alumno.getNotas().add(nota1);
                        alumnoService.save(alumno);
                    }
                }
            }

        }

    }



    @GetMapping("/add/alumno")
    public String registroalumno(Model model)
    {
        List<Curso> cursos = (List<Curso>) cursoService.getAll();
        model.addAttribute("cursos", cursos);
        model.addAttribute("alumno", new Alumno());
        return "add-alumno";
    }


    @PostMapping("/save/alumno")
    public String checkPersonInfo(@Valid Alumno alumno, BindingResult bindingResult) {
        if (bindingResult.hasErrors()) {
            return "add-alumno";
        }else {

            alumno.setCurso(cursoService.get(alumno.getIdCurso()));

            Curso curso = cursoService.get(alumno.getIdCurso());

            List<Asignatura> asignaturas = curso.getAsignaturas();
            int notas = 4;
            for(Asignatura asignatura : asignaturas) {
                if(asignatura.nombre.equals("arte") || asignatura.nombre.equals("tecnologia") ){
                    notas = 3;
                }
                for (int i = 1; i <= notas; i++){
                    Nota nota = new Nota();
                    nota.setValor((double) 0);
                    nota.setPeriodo("Primer periodo");
                    nota.setAsignatura(asignatura);
                    notaService.save(nota);
                    alumno.getNotas().add(nota);
                }
            }

            alumnoService.save(alumno);
        }
        return "redirect:/add/alumno";
    }


    @PostMapping("/guardar-nota")
    public String saveNote(HttpSession session, @RequestParam("nuevoValor") Double valor,@RequestParam("idNota") Long id ) {
        logger.info("HELLOOOOOOOOOOOO");
        notaService.guardarNota(id, valor);
        Long cursoId = getNivel(session);
        return "redirect:/notas/"+cursoId;
    }


    private void saveIdAsig(HttpServletRequest request, Long nivel){
        Long nvl = getIdAsig(request.getSession());
        nvl = nivel;
        request.getSession().setAttribute("idAsignaturaGuardada", nvl);

    }

    private Long getIdAsig(HttpSession session){
        Long nvl = (Long) session.getAttribute("idAsignaturaGuardada");
        return nvl;
    }



    private List<String> cantidadDeNotas(){
        List<Datos> datos = (List<Datos>) datosService.getAll();
        int cantNotas = datos.get(datos.size()-1).cantidadNotas;
        List<String> notas = new ArrayList<>();
        for(int i = 1; i <= cantNotas; i++){
            notas.add("Nota "+i);
        }
        return notas;
    }

    private int buscarRechazados(List<Recurso> lista) {
        int flag = 0;
        for (Recurso recurso : lista) {
            if (recurso.getEstado().equals("rechazado")){
                flag++;
            }
        }
        return flag;
    }

    private int buscarRechazadosArchivo(List<Archivo> lista) {
        int flag = 0;
        for (Archivo archivo : lista) {
            if (archivo.getEstado().equals("rechazado")){
                flag++;
            }
        }
        return flag;
    }

    private int buscarSinRevisar(List<Recurso> lista) {
        int flag = 0;
        for (Recurso recurso : lista) {
            if (recurso.getEstado().equals("sin revisar")){
                flag++;
            }
        }
        return flag;
    }

    private int buscarSinRevisarArchivo(List<Archivo> lista) {
        int flag = 0;
        for (Archivo archivo : lista) {
            if (archivo.getEstado().equals("sin revisar")){
                flag++;
            }
        }
        return flag;
    }

    @GetMapping("/usuarios")
    public String profesores(HttpServletRequest request, Model model){
        if (request.isUserInRole("ROLE_PROF")) {
            return "redirect:/cursos";
        }else {
            List<User> profesores = userService.getAllProfesores();
            /*
            if(usuarioLogeado.getUltimaConexion() != null) {
                Date ultima = usuarioLogeado.getUltimaConexion();

                List<User> subidos = new ArrayList<>();
                for (User profesor : profesores) {
                    Date date = profesor.getUltimaConexion();
                    if (date != null) {
                        if (date.before(ultima)) {
                            subidos.add(profesor);
                        }
                    }
                }
                model.addAttribute("listaSubidos", subidos);
            }else {
                model.addAttribute("list", profesores);
            }*/
            model.addAttribute("list", profesores);
            return "usuarios";
        }
    }

    @GetMapping("/board")
    public List<Archivo> tabla( Model model, HttpServletRequest request) {
        List<Archivo> lista = null;
        Long idCursoElegido = getNivel(request.getSession());
        //verificar si es admin o user
        saveVersionTable(request, "planificaciones");

        User usuarioLogeado = userService.buscaridUsuarioLogeado();
        if(usuarioLogeado != null) {
            if (usuarioLogeado.getArea().equals("admin")) {
                 lista = (List<Archivo>) archivoService.getAllByNivelId(idCursoElegido, idUsuarioElegido);

                model.addAttribute("listaArchivos", lista);
                model.addAttribute("totalRechazados", buscarRechazadosArchivo(lista));
                model.addAttribute("totalSinRevisar", buscarSinRevisarArchivo(lista));
                model.addAttribute("totalArchivos",                lista.size());
            } else {
                lista = (List<Archivo>) archivoService.getAllByNivelId(idCursoElegido, usuarioLogeado.getId());
                model.addAttribute("listaArchivos", lista);
                model.addAttribute("totalRechazados", buscarRechazadosArchivo(lista));
                model.addAttribute("totalSinRevisar", buscarSinRevisarArchivo(lista));
                model.addAttribute("totalArchivos",                lista.size());
                userService.updateNotification(usuarioLogeado.getId(), 0);

                notificacionService.deleteAll();

            }
        }/*else{
            lista = recursoService.getAllByNivelFecha(idCursoElegido, fechaElegida)
            model.addAttribute("listaArchivos", lista);

        }*/
        model.addAttribute("version", "archivos");
        model.addAttribute("curso", cursoService.getById(idCursoElegido));

        return lista;
    }


    @GetMapping("/recursos")
    public String tablaRecursos( Model model, HttpServletRequest request) {
        Long idCursoElegido = getNivel(request.getSession());
        //false significa que estoy en recursos
        saveVersionTable(request, "recursos");

        //verificar si es admin o user
        User usuarioLogeado = userService.buscaridUsuarioLogeado();
        if(usuarioLogeado != null) {
            if (usuarioLogeado.getArea().equals("admin")) {
                List<Recurso> lista = recursoService.getAllByNivelId(idCursoElegido, idUsuarioElegido);
                model.addAttribute("listaArchivos", lista);
                model.addAttribute("totalRechazados", buscarRechazados(lista));
                model.addAttribute("totalSinRevisar", buscarSinRevisar(lista));
                model.addAttribute("totalArchivos",                lista.size());
            } else if (usuarioLogeado.getArea().equals("profesor")) {
                userService.updateNotification(usuarioLogeado.getId(), 0);
                notificacionService.deleteAll();
                List<Recurso> lista = recursoService.getAllByNivelId(idCursoElegido, usuarioLogeado.getId());
                model.addAttribute("listaArchivos", lista);
                model.addAttribute("totalSinRevisar", buscarSinRevisar(lista));
                model.addAttribute("totalRechazados", buscarRechazados(lista));
                model.addAttribute("totalArchivos",                lista.size());
            }
        }else{
            //si es un usuario no identificado
            model.addAttribute("listaArchivos", recursoService.getAllByNivelFecha(idCursoElegido, fechaElegida));
        }
        model.addAttribute("version", "recursos");
        model.addAttribute("curso", cursoService.getById(idCursoElegido));
        return "board";
    }

    @GetMapping("/comentario/{id}")
    public String updateComentario (@PathVariable Long id, Model model, HttpSession session){
        //pasamos el id del archivo al campo de esta controlador que guardara ese valor
        idArchivoModificado = id;
        Comentario comentario = new Comentario();
        String tablaVersion = getVerionTabla(session);
        if(tablaVersion.equals("planificaciones")) {
            comentario.setTexto(archivoService.get(id).getComentario());
        }else{
            comentario.setTexto(recursoService.get(id).getComentario());
        }
        model.addAttribute("comentario", comentario);
        return "add-comentario";

    }

    @GetMapping("perform_logout")
    public String salir(){
        User usuarioLogeado = userService.buscaridUsuarioLogeado();

        if(usuarioLogeado.getArea().equals("admin")){
            userService.actualizarUltimaConexion(usuarioLogeado.getId(), dameFecha());
        }
        return "/";
    }



    @PostMapping("/upload/comentario")
    public String setComentario(Comentario comentario, Model model, HttpSession session){
        Notificacion notificacion = new Notificacion();
        Archivo archivo;
        Recurso recurso;
        String nombreArchivo;
        String cursoArchivo;
        User usuarioLogeado = userService.buscaridUsuarioLogeado();
        String tablaVersion = getVerionTabla(session);

        notificacion.setAutor(usuarioLogeado.getNombre()+ " "+usuarioLogeado.getApellido());
        if(tablaVersion.equals("planificaciones")) {
            archivo = archivoService.get(idArchivoModificado);
            nombreArchivo = archivo.getNombreArchivo();
            cursoArchivo = cursoService.get(archivo.getNivel()).getNombre();
            //actualizamos el campo comentario de tipo String en el archivo
            archivoService.updateComentario(comentario.getTexto(), idArchivoModificado);
        }else{
            recurso = recursoService.get(idArchivoModificado);
            nombreArchivo = recurso.getNombreArchivo();
            cursoArchivo = cursoService.get(recurso.getNivel()).getNombre();
            recursoService.updateComentario(comentario.getTexto(), idArchivoModificado);
        }

        notificacion.setNombreArchivo(nombreArchivo);
        notificacion.setCurso(cursoArchivo);
        notificacion.setIdUsuario(idUsuarioElegido);
        notificacionService.guardar(notificacion);
        //sumamos 1 a sus notifiaciones
        int notify = usuarioElegido.getNotificacion();
        notify ++;
        //guardamos el comentario hasta que el archivo sea aprobado
        //actualizamos las notificaciones del usuario elegido
        userService.updateNotification(usuarioElegido.getId(), notify);

        if(tablaVersion.equals("planificaciones")) {
            return "redirect:/board";
        }else{
            return "redirect:/recursos";
        }
    }


    @RequestMapping("/archivos")
    public String mostrarArchivos(Model model) {
        model.addAttribute("listaArchivos", archivoService.getAll());
        return "archivos";
    }

    @GetMapping("/add/file")
    public String guardarArchivo(HttpSession session) {
        String tablaVersion = getVerionTabla(session);
        if(tablaVersion.equals("planificaciones")){
            return "form-archivo";
        }else{
            return "recurso";
        }
    }

    @GetMapping("/up/{id}")
    public String uppp (@PathVariable Long id) {
        idArchivoActualizar = id;
        return "update-archivo";
    }

    @PostMapping("/update")
    public String updateFile (@RequestParam("file") MultipartFile file, Model model, HttpSession session) {
        Archivo archivo;
        Recurso recurso;
        FileArc fileArc;
        String fileName = StringUtils.cleanPath(file.getOriginalFilename());
        String tablaVersion = getVerionTabla(session);

        if(tablaVersion.equals("planificaciones")) {
            archivo = archivoService.get(idArchivoActualizar);
            archivo.setNombreArchivo(fileName);
            archivoService.updateNombreArchivo(archivo);
            fileArc =  datoService.get(archivo.getFileArc().getId());
        }else{
            recurso = recursoService.get(idArchivoActualizar);
            recurso.setNombreArchivo(fileName);
            recursoService.updateNombreArchivo(recurso);
            fileArc =  datoService.get(recurso.getFilearc().getId());
        }

        fileArc.setContentType(file.getContentType());
        fileArc.setNombreArchivo(fileName);

        try {
            fileArc.setDatosDelArchivo(file.getBytes());
        } catch (IOException e) {
            e.printStackTrace();
        }

        datoService.udpdatefileArc(idArchivoActualizar, fileArc);
        if(tablaVersion.equals("planificaciones")) {
            return "redirect:/board";
        }else{
            return  "redirect:/recursos";
        }
    }

    @PostMapping("/upload")
    public String uploadFile(HttpServletRequest request, @RequestParam("des") String des,@RequestParam("materia") String materia,@RequestParam("categoria") String categoria, @RequestParam("tipo") String tipo, @RequestParam("file") MultipartFile[] files, RedirectAttributes attributes, Model model) {
        Long idCursoElegido = getNivel(request.getSession());

        if (files.length == 0) {
            attributes.addFlashAttribute("message", "Elija un archivo válido.");
            return "redirect:/addfile";
        }
        /*
        if (!fileSizeIsHigher(file)) {
            attributes.addFlashAttribute("message", "Elija un archivo de tamaño menor a 40MB.");
            return "redirect:/addfile";
        }*/
        // normalize the file path
        for (MultipartFile file: files){
            String fileName = StringUtils.cleanPath(file.getOriginalFilename());
            FileArc fileArc =  new FileArc();
            fileArc.setNombreArchivo(fileName);
            fileArc.setContentType(file.getContentType());
            Archivo archivo = new Archivo();
            archivo.setFecha(dameFecha());
            if(!des.isEmpty()) {
                archivo.setDescripcion(des);
            }
            archivo.setNivel(idCursoElegido);
            archivo.setCategoria(categoria);
            archivo.setMateria(materia);
            User usuarioLogeado = userService.buscaridUsuarioLogeado();
            archivo.setIdUsuario(usuarioLogeado.getId());
            archivo.setNombreArchivo(fileName);
            archivo.setTipo(tipo);
            archivo.setFileArc(fileArc);
            try {
                fileArc.setDatosDelArchivo(file.getBytes());
            } catch (IOException e) {
                e.printStackTrace();
            }
            //Path path = Paths.get(UPLOAD_DIR + fileName);
            //Files.copy(file.getInputStream(), path, StandardCopyOption.REPLACE_EXISTING);
            datoService.save(fileArc);
            archivoService.save(archivo, usuarioLogeado.getNombre());
        }

        attributes.addFlashAttribute("message", "You successfully uploaded ");

        return "redirect:/board";
    }


    @PostMapping("/upload/recurso")
    public String uploadFile(HttpServletRequest request, @RequestParam("date") @DateTimeFormat(iso = DateTimeFormat.ISO.DATE) Date date, @RequestParam("materia") String materia, @RequestParam("file") MultipartFile[] files, RedirectAttributes attributes, Model model) {
        logger.info("HELLO");
        Long idCursoElegido = getNivel(request.getSession());
        User usuarioLogeado = userService.buscaridUsuarioLogeado();

        if (files.length == 0) {
            attributes.addFlashAttribute("message", "Elija un archivo válido.");
            return "redirect:/recurso";
        }
        /*
        if (!fileSizeIsHigher(file)) {
            attributes.addFlashAttribute("message", "Elija un archivo de tamaño menor a 40MB.");
            return "redirect:/addfile";
        }*/
        // normalize the file path
        for (MultipartFile file: files){
            String fileName = StringUtils.cleanPath(file.getOriginalFilename());
            FileArc fileArc =  new FileArc();
            fileArc.setNombreArchivo(fileName);
            fileArc.setContentType(file.getContentType());
            Recurso recurso = new Recurso();

            recurso.setFecha(date);
            logger.info("CURSO ELEGIDO", idCursoElegido);
            recurso.setNivel(idCursoElegido);
            recurso.setMateria(materia);
            logger.info("CURSO ELEGIDO", usuarioLogeado.getId());
            recurso.setIdUsuario(usuarioLogeado.getId());
            recurso.setNombreArchivo(fileName);
            recurso.setFilearc(fileArc);
            try {
                fileArc.setDatosDelArchivo(file.getBytes());
            } catch (IOException e) {
                e.printStackTrace();
            }
            //Path path = Paths.get(UPLOAD_DIR + fileName);
            //Files.copy(file.getInputStream(), path, StandardCopyOption.REPLACE_EXISTING);
            datoService.save(fileArc);
            recursoService.save(recurso,usuarioLogeado.getNombre());

            userService.actualizarUltimaConexion(usuarioLogeado.getId(), dameFecha());
        }

        attributes.addFlashAttribute("message", "You successfully uploaded ");

        return "redirect:/recursos";
    }

    private Date dameFecha() {
        Date date = new Date();
        DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
        dateFormat.format(date);
        return date;
    }


}
