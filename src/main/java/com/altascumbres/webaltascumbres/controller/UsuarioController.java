package com.altascumbres.webaltascumbres.controller;


import com.altascumbres.webaltascumbres.entity.User;
import com.altascumbres.webaltascumbres.service.ArchivoService;
import com.altascumbres.webaltascumbres.service.CursoService;
import com.altascumbres.webaltascumbres.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.propertyeditors.StringTrimmerEditor;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.config.annotation.ViewControllerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import java.util.Date;

@Controller
public class UsuarioController implements WebMvcConfigurer {
    @Autowired
    UserService userService;
    @Autowired
    CursoService cursoService;

    @Autowired
    ArchivoService archivoService;

    @InitBinder
    public void initBinder(WebDataBinder webDataBinder){
        StringTrimmerEditor stringTrimmerEditor = new StringTrimmerEditor(true);
        webDataBinder.registerCustomEditor(String.class,stringTrimmerEditor);
    }


    @Override
    public void addViewControllers(ViewControllerRegistry registry) {
        registry.addViewController("/usuarios").setViewName("usuarios");
    }

    @PostMapping("/save")
    public String checkPersonInfo(@Valid User user, BindingResult bindingResult) {
        if (bindingResult.hasErrors()) {
            return "add-user";
        }else{
            this.userService.save(user);
        }
        return "redirect:/login";
    }



    /*
    @GetMapping("/save/{id}")
    public String showSave(@PathVariable("id") Long id, Model model) {
        if (id != null && id != 0) {
            model.addAttribute("user", userService.get(id));
        } else {
            model.addAttribute("user", new User());
        }
        return "add-user";
    }*/

    @GetMapping("/register/user")
    public String showSave( Model model) {
        model.addAttribute("user", new User());
        return "add-user";
    }




    @GetMapping("/delete/user/{id}")
    public String delete(@PathVariable Long id, Model model) {
        userService.delete(id);
        return "redirect:/usuarios";
    }




}
