package com.altascumbres.webaltascumbres.entity;


import javax.persistence.*;
import java.util.Date;

@Entity
public class Recurso {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)//para el id automatico en la BD
    public Long id;

    public Long idUsuario;

    @OneToOne(cascade=CascadeType.ALL, fetch=FetchType.LAZY)
    public FileArc filearc;

    public String nombreArchivo;

    public String materia;

    //estado puede ser aceptado, rechazado, sin revisar, actualizado
    public String estado;

    //variable que guarda el id del curso al que pertenece
    public Long nivel;

    public String comentario;

    //fecha del primer dia de la semana
    @Temporal(TemporalType.DATE)
    public Date fecha;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getIdUsuario() {
        return idUsuario;
    }

    public void setIdUsuario(Long idUsuario) {
        this.idUsuario = idUsuario;
    }

    public FileArc getFilearc() {
        return filearc;
    }

    public void setFilearc(FileArc filearc) {
        this.filearc = filearc;
    }

    public String getNombreArchivo() {
        return nombreArchivo;
    }

    public void setNombreArchivo(String nombreArchivo) {
        this.nombreArchivo = nombreArchivo;
    }

    public String getMateria() {
        return materia;
    }

    public void setMateria(String materia) {
        this.materia = materia;
    }

    public String getEstado() {
        return estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }

    public Long getNivel() {
        return nivel;
    }

    public void setNivel(Long nivel) {
        this.nivel = nivel;
    }

    public String getComentario() {
        return comentario;
    }

    public void setComentario(String comentario) {
        this.comentario = comentario;
    }

    public Date getFecha() {
        return fecha;
    }

    public void setFecha(Date fecha) {
        this.fecha = fecha;
    }
}
