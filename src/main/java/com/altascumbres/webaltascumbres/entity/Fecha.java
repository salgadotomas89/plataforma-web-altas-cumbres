package com.altascumbres.webaltascumbres.entity;

import java.util.Date;

public class Fecha {

    public String fecha;

    public Date fechaDate;

    public String getFecha() {
        return fecha;
    }

    public void setFecha(String fecha) {
        this.fecha = fecha;
    }

    public Date getFechaDate() {
        return fechaDate;
    }

    public void setFechaDate(Date fechaDate) {
        this.fechaDate = fechaDate;
    }
}
