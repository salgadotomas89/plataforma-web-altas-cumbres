package com.altascumbres.webaltascumbres.entity;


import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import javax.persistence.*;
import java.util.Date;
import java.util.List;
import java.util.Set;

@Entity
public class User {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)//para el id automatico en la BD
    public Long id;

    @NotBlank(message = "Nombre es obligatorio")
    public String nombre;

    @NotBlank(message = "Apellido es obligatorio")
    public String apellido;

    public int notificacion;


    @NotBlank(message = "Género obligatorio")
    public String genero;

    @NotBlank(message = "Nombre de usuario es obligatorio")
    public String username;

    @NotBlank(message = "Email obligatorio")
    @Email(message = "Ingrese un email valido")
    public String email;

    @NotBlank(message = "Contraseña es obligatoria")
    public String password;

    @ManyToMany(fetch=FetchType.EAGER)
    public List<Asignatura> asignaturas;

    @NotBlank(message = "Area es obligatoria")
    public String area;

    //estado se refiere a si trabaja o no actualmente
    public boolean estado;

    public String profesion;


    @Column
    private boolean enabled;

    @ManyToMany(fetch=FetchType.EAGER)
    private Set<Authority> authority;

    private Date ultimaConexion;

    private int ciclo;

    public int getCiclo() {
        return ciclo;
    }

    public void setCiclo(int ciclo) {
        this.ciclo = ciclo;
    }

    public Date getUltimaConexion() {
        return ultimaConexion;
    }

    public void setUltimaConexion(Date ultimaConexion) {
        this.ultimaConexion = ultimaConexion;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String contraseña) {
        this.password = contraseña;
    }

    public String getApellido() {
        return apellido;
    }

    public void setApellido(String apellido) {
        this.apellido = apellido;
    }

    public String getGenero() {
        return genero;
    }

    public void setGenero(String genero) {
        this.genero = genero;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public boolean isEnabled() {
        return enabled;
    }

    public void setEnabled(boolean enabled) {
        this.enabled = enabled;
    }

    public Set<Authority> getAuthority() {
        return authority;
    }

    public void setAuthority(Set<Authority> authority) {
        this.authority = authority;
    }

    public int getNotificacion() {
        return notificacion;
    }

    public void setNotificacion(int notificacion) {
        this.notificacion = notificacion;
    }

    public String getArea() {
        return area;
    }

    public void setArea(String area) {
        this.area = area;
    }

    public boolean isEstado() {
        return estado;
    }

    public void setEstado(boolean estado) {
        this.estado = estado;
    }

    public List<Asignatura> getAsignaturas() {
        return asignaturas;
    }

    public void setAsignaturas(List<Asignatura> asignaturas) {
        this.asignaturas = asignaturas;
    }

    public String getProfesion() {
        return profesion;
    }

    public void setProfesion(String profesion) {
        this.profesion = profesion;
    }
}
