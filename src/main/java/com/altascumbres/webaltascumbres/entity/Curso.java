package com.altascumbres.webaltascumbres.entity;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
public class Curso {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    public Long id;

    public String nombre;

    public String descripcion;

    public Long idProfesorJefe;

    public int ciclo;

    @ManyToMany
    public List<Asignatura> asignaturas = new ArrayList<>();

    public int getCiclo() {
        return ciclo;
    }

    public void setCiclo(int ciclo) {
        this.ciclo = ciclo;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public Long getIdProfesorJefe() {
        return idProfesorJefe;
    }

    public void setIdProfesorJefe(Long idProfesorJefe) {
        this.idProfesorJefe = idProfesorJefe;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public List<Asignatura> getAsignaturas() {
        return asignaturas;
    }

    public void setAsignaturas(List<Asignatura> asignaturas) {
        this.asignaturas = asignaturas;
    }
}
