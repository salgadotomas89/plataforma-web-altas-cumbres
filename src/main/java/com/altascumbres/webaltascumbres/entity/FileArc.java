package com.altascumbres.webaltascumbres.entity;

import javax.persistence.*;

@Entity
public class FileArc {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)//para el id automatico en la BD
    public Long id;

    @Lob
    public byte[] datosDelArchivo;

    private String contentType;

    public String nombreArchivo;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }


    public byte[] getDatosDelArchivo() {
        return datosDelArchivo;
    }

    public void setDatosDelArchivo(byte[] datos) {
        this.datosDelArchivo = datos;
    }

    public String getContentType() {
        return contentType;
    }

    public void setContentType(String contentType) {
        this.contentType = contentType;
    }

    public String getNombreArchivo() {
        return nombreArchivo;
    }

    public void setNombreArchivo(String nombreArchivo) {
        this.nombreArchivo = nombreArchivo;
    }
}
